EJERCICIO JAVA RETRATO ROBOT


C.F.G.S Desarrollo de Aplicaciones Web
Módulo de Programación - Curso 20-21
Autor: Mª Eugenia Alvarez Alvarez


Programa para crear retratos robot con cadenas de caracteres.
Una vez creado el programa, el usuario podrá crear retratos robot de una forma sencilla. 
Los retratos se crean eligiendo los rasgos de cada facción con las opciones siguientes:

PELO					OJOS				NARIZ		
1. WWWWWWWWW			1. | 0	 0 |		1. @	J	@
2. \\\//////			2. |-(·	·)-|		2. {	"	}
3. |"""""""|			3. |-(O	O)-|		3. [	j	]
4. |||||||||			4. | \	 / |		4. <	-	>

BOCA					BARBILLA							
1. |  ===  |        	1. \_______/
2. |   -   |			2. \,,,,,,,/		
3. |  ___  |		
4. |  ---  |				


Una vez elegidos los rasgos de cada facción, se mostrará el retrato por pantalla. Por ejemplo, las opciones 3, 2, 1, 4 y 2 para pelo, ojos, nariz, boca y barbilla respectivamente, muestran el siguiente retrato robot:

|"""""""|
|-(· ·)-|
@	J	@
|  ___  |
\,,,,,,,/
